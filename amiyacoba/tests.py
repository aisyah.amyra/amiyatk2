from django.test import TestCase

# Create your tests here.
def test_books_is_exist(self):
    response = Client().get('searchMovie/')
    self.assertEqual(response.status_code, 200)

def test_using_perpus(self):
    found = resolve('searchMovie/')
    self.assertEqual(found.func, searchMovie)
    
def test_using_perpus_template(self):
    response = Client().get('searchMovie/')
    self.assertTemplateUsed(response, 'searchMovie.html')

def test_books_has_title(self):
    response = Client().get('searchMovie/')
    html_response = response.content.decode('utf8')
    self.assertIn("Movie goers", html_response)
