from django.urls import path
from . import views

app_name = 'amiyacoba'

urlpatterns = [
    path('searchMovie/', views.searchMovie, name='searchMovie'),

]
