from django.shortcuts import render
from .models import Movies

# Create your views here.

def searchMovie(request):
    movies = None

    if 'search' in request.GET.keys() :
        keyword = request.GET['search'] 
        movies = Movies.objects.filter(title = keyword)

    return render(request, 'searchMovie.html',{ 'Movies' : movies})
